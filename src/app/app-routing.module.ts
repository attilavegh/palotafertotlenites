import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FragmentContainerComponent } from './pages/fragment-container/fragment-container.component';
import { OfferRequestComponent } from './pages/offer-request/offer-request.component';

const routes: Routes = [
  { path: '', component: FragmentContainerComponent, pathMatch: 'full' },
  { path: 'ajanlatkeres', component: OfferRequestComponent },
  { path: '**', redirectTo: '/' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    onSameUrlNavigation: 'reload',
    anchorScrolling: 'enabled',
    scrollOffset: [0, 64]
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
