import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { FooterSectionComponent } from './components/footer/footer-section/footer-section.component';
import { MenuItemComponent } from './components/header/menu-item/menu-item.component';
import { TeaserContentComponent } from './components/teaser-content/teaser-content.component';
import { IconComponent } from './components/icon/icon.component';
import { HamburgerIconComponent } from './components/header/hamburger-icon/hamburger-icon.component';
import { FragmentContainerComponent } from './pages/fragment-container/fragment-container.component';
import { OfferRequestComponent } from './pages/offer-request/offer-request.component';
import { ActionFeedbackComponent } from './components/action-feedback/action-feedback.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { InputFieldComponent } from './components/input-field/input-field.component';
import { ButtonComponent } from './components/button/button.component';
import { ServicesComponent } from './pages/fragment-container/services/services.component';
import { DisinfectantComponent } from './pages/fragment-container/disinfectant/disinfectant.component';
import { AreasComponent } from './pages/fragment-container/areas/areas.component';
import { TextContentComponent } from './components/text-content/text-content.component';
import { ListContentComponent } from './components/list-content/list-content.component';
import { ListItemComponent } from './components/list-content/list-item/list-item.component';
import { IframeComponent } from './components/iframe/iframe.component';
import { ImageComponent } from './components/image/image.component';
import { ImageContentComponent } from './components/image-content/image-content.component';
import { HighlightedOfferRequestButtonComponent } from './components/highlighted-offer-request-button/highlighted-offer-request-button.component';
import { IntersectionDirective } from './directives/intersection/intersection.directive';
import { FragmentDirective } from './directives/fragment/fragment.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FooterSectionComponent,
    MenuItemComponent,
    TeaserContentComponent,
    IconComponent,
    HamburgerIconComponent,
    FragmentContainerComponent,
    OfferRequestComponent,
    ActionFeedbackComponent,
    SpinnerComponent,
    InputFieldComponent,
    ButtonComponent,
    ServicesComponent,
    DisinfectantComponent,
    AreasComponent,
    TextContentComponent,
    ListContentComponent,
    ListItemComponent,
    IframeComponent,
    ImageComponent,
    ImageContentComponent,
    HighlightedOfferRequestButtonComponent,

    IntersectionDirective,

    FragmentDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
