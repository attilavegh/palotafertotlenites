import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'pf-action-feedback',
  templateUrl: './action-feedback.component.html',
  styleUrls: ['./action-feedback.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionFeedbackComponent {

  @Input() type: 'success' | 'error';
  @Input() content: string;

  constructor() {
  }
}
