import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { FooterSection } from '../../../services/content/content.model';

@Component({
  selector: 'pf-footer-section',
  templateUrl: './footer-section.component.html',
  styleUrls: ['./footer-section.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterSectionComponent {

  @Input() section: FooterSection;
  @Input() indicatorType: 'square' | 'circle' = 'square';

  constructor() {
  }
}
