import { ChangeDetectionStrategy, Component } from '@angular/core';

import { ContentService } from '../../services/content/content.service';

@Component({
  selector: 'pf-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent {

  constructor(private contentService: ContentService) {
  }

  get content() {
    return this.contentService.data.footer;
  }
}
