import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'pf-hamburger-icon',
  templateUrl: './hamburger-icon.component.html',
  styleUrls: ['./hamburger-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HamburgerIconComponent {

  @Input()
  open: boolean;

  constructor() {}

}
