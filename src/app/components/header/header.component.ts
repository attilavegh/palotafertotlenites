import { ChangeDetectionStrategy, Component, ElementRef, HostListener, ViewChild } from '@angular/core';

import { ContentService } from '../../services/content/content.service';
import { FragmentNavigationService } from '../../services/fragment-navigation/fragment-navigation.service';

@Component({
  selector: 'pf-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
  @ViewChild('navigation', { static: true }) navigationBar: ElementRef;

  isMobileHeaderOpen = false;
  shouldAnimate = true;
  shouldHaveShadow = false;

  activeFragment$ = this.fragmentNavigation.currentFragment$;

  constructor(private contentService: ContentService,
              private fragmentNavigation: FragmentNavigationService) {
  }

  @HostListener('window:scroll')
  onWindowScroll() {
    this.shouldHaveShadow = window.pageYOffset > 0;
  }

  toggleMobileHeader() {
    this.isMobileHeaderOpen = !this.isMobileHeaderOpen;
  }

  onLinkSelected() {
    this.isMobileHeaderOpen = false;
    this.shouldAnimate = false;

    setTimeout(() => {
      this.shouldAnimate = true;
    });
  }

  get mobileMenuHeight() {
    return this.navigationBar.nativeElement.scrollHeight;
  }

  get content() {
    return this.contentService;
  }
}
