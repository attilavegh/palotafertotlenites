import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { MenuItem } from '../../../services/content/content.model';

@Component({
  selector: 'pf-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemComponent {

  @Input() item: MenuItem;
  @Input() active: boolean;
  @Input() isLast: boolean;

  constructor() {
  }
}
