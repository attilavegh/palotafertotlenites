import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HighlightedOfferRequestButtonComponent } from './highlighted-offer-request-button.component';

describe('HighlightedOfferRequestButtonComponent', () => {
  let component: HighlightedOfferRequestButtonComponent;
  let fixture: ComponentFixture<HighlightedOfferRequestButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HighlightedOfferRequestButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HighlightedOfferRequestButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
