import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pf-highlighted-offer-request-button',
  templateUrl: './highlighted-offer-request-button.component.html',
  styleUrls: ['./highlighted-offer-request-button.component.scss']
})
export class HighlightedOfferRequestButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
