import { ChangeDetectionStrategy, Component, ElementRef, HostBinding, Input, Renderer2, OnInit } from '@angular/core';

declare const require: any;

@Component({
  selector: 'pf-icon',
  template: '',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent implements OnInit {

  @Input() icon: string;

  @HostBinding('attr.aria-hidden') @Input() ariaHidden = true;

  constructor(private renderer: Renderer2, private elementRef: ElementRef) {}

  ngOnInit(): void {
    if (this.icon) {
      try {
        this.setSvgElement(require(`!svg-inline-loader!../../../assets/svgs/${this.icon}.svg`));
      } catch (e) {
        console.error(`Icon missing: ${this.icon}`);
      }
    } else {
      this.clearSvgElement();
    }
  }

  setSvgElement(svgSource: string): void {
    this.clearSvgElement();
    const svgElement = this.createSvgFromString(svgSource);
    if (svgElement) {
      this.renderer.appendChild(this.elementRef.nativeElement, svgElement);
    }
  }

  clearSvgElement(): void {
    const layoutElement = this.elementRef.nativeElement;
    const childCount = layoutElement.childNodes.length;
    for (let i = 0; i < childCount; i++) {
      this.renderer.removeChild(layoutElement, layoutElement.childNodes[i]);
    }
  }

  private createSvgFromString(svgSource: string): SVGElement {
    const div = this.renderer.createElement('div');
    div.innerHTML = svgSource;
    return div.querySelector('svg');
  }
}
