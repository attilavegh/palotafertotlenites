import { ChangeDetectionStrategy, Component, ElementRef, HostBinding, Input, OnInit } from '@angular/core';

import { IframeInjectorService } from '../../services/iframe-injector/iframe-injector.service';

@Component({
  selector: 'pf-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IframeComponent implements OnInit {

  @HostBinding('style.width.px')
  @Input() width: string;

  @HostBinding('style.height.px')
  @Input() height: string;

  @Input() title: string;
  @Input() src: string;
  @Input() allow: string;
  @Input() injectionDelay = 0;

  constructor(private iframeInjector: IframeInjectorService,
              private element: ElementRef) {
  }

  ngOnInit() {
    this.iframeInjector.inject({
      title: this.title,
      src: this.src,
      parentElement: this.element.nativeElement,
      height: this.height,
      width: this.width,
      allow: this.allow,
      injectionDelay: this.injectionDelay
    }).subscribe();
  }
}
