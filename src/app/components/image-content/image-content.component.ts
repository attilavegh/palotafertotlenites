import { AfterViewInit, Component, ElementRef, Input, Renderer2, ViewChild } from '@angular/core';
import { CompatibilityService } from '../../services/compatibility/compatibility.service';

@Component({
  selector: 'pf-image-content',
  templateUrl: './image-content.component.html',
  styleUrls: ['./image-content.component.scss']
})
export class ImageContentComponent {
  @ViewChild('imageWrapper') imageWrapper: ElementRef;

  @Input() title: string;
  @Input() imageName: string;
  @Input() text: string;

  isOpen = false;

  constructor(private compatibility: CompatibilityService,
              private renderer: Renderer2) {
  }

  toggle() {
    this.isOpen = !this.isOpen && !!this.text;
  }

  loadBackgroundImage() {
    this.compatibility.hasWebPSupport$.subscribe((hasWebPSupport: boolean) => {
      if (hasWebPSupport) {
        this.renderer.setStyle(this.imageWrapper.nativeElement, 'background-image', `url("../../../assets/images-webp/${this.imageName}.webp")`);
      } else {
        this.renderer.setStyle(this.imageWrapper.nativeElement, 'background-image', `url("../../../assets/images/${this.imageName}.jpg")`);
      }
    });
  }
}
