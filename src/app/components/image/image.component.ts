import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { CompatibilityService } from '../../services/compatibility/compatibility.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'pf-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageComponent {
  @Input() name: string;
  @Input() alt: string;

  src$ = this.compatibility.hasWebPSupport$.pipe(
    map((hasWebPSupport: boolean) => {
      const root = '/assets/';
      const folder = hasWebPSupport ? 'images-webp/' : 'images/';
      const extension = hasWebPSupport ? '.webp' : '.jpg';

      return root + folder + this.name + extension;
    })
  );

  constructor(private compatibility: CompatibilityService) {
  }
}
