import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldComponent } from './input-field.component';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  template: `
    <pf-input-field
        [formControl]="control"
    ></pf-input-field>
  `
})
class InputFieldWrapperComponent {
  control = new FormControl('');

  constructor() {
  }
}

describe('InputFieldComponent', () => {
  let wrapperComponent: InputFieldWrapperComponent;
  let fixture: ComponentFixture<InputFieldWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [
        InputFieldWrapperComponent,
        InputFieldComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldWrapperComponent);
    wrapperComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(wrapperComponent).toBeTruthy();
  });
});
