import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, Optional, Self } from '@angular/core';
import {
  ControlValueAccessor,
  NgControl,
} from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'pf-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputFieldComponent implements ControlValueAccessor, OnInit, OnDestroy {

  @Input() label;
  @Input() placeholder = '';
  @Input() type: 'text' | 'email' | 'tel' | 'textarea' = 'text';
  @Input() isAutocompleteEnabled = false;

  _value: string;
  isFocused = false;
  isDisabled = false;

  private statusSubscription: Subscription;

  onChange = (_: any) => {};

  constructor(@Self() @Optional() public control: NgControl,
              private changeDetectorRef: ChangeDetectorRef) {
    this.control.valueAccessor = this;
  }

  ngOnInit() {
    this.statusSubscription = this.control.statusChanges.subscribe(() => this.changeDetectorRef.markForCheck());
  }

  ngOnDestroy() {
    this.statusSubscription.unsubscribe();
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
    this.changeDetectorRef.markForCheck();
  }

  writeValue(value: any) {
    this.value = !!value ? value : '';
    this.changeDetectorRef.markForCheck();
  }

  onFocus() {
    this.isFocused = true;
  }

  onBlur() {
    this.isFocused = false;
  }

  onKeyup(event: Event) {
    this.value = (event.target as HTMLInputElement).value;
  }

  get isTextarea() {
    return this.type === 'textarea';
  }

  get hasError() {
    return !this.control.pristine && !!this.control.errors;
  }

  get isEmpty() {
    return this.value === '';
  }

  get value() {
    return this._value;
  }

  set value(value: any) {
    this._value = value;
    this.onChange(value);
  }
}
