import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListContentComponent } from './list-content.component';
import { ListItemComponent } from './list-item/list-item.component';
import { IconComponent } from '../icon/icon.component';
import { ContentService } from '../../services/content/content.service';

describe('ListContentComponent', () => {
  let component: ListContentComponent;
  let fixture: ComponentFixture<ListContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListContentComponent, ListItemComponent, IconComponent ],
      providers: [ ContentService ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
