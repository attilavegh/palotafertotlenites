import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { ListItem } from '../../services/content/content.model';

@Component({
  selector: 'pf-list-content',
  templateUrl: './list-content.component.html',
  styleUrls: ['./list-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListContentComponent {

  @Input() items: ListItem[];

  constructor() {
  }
}
