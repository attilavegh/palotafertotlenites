import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { ListItem } from '../../../services/content/content.model';

@Component({
  selector: 'pf-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListItemComponent {

  @Input() item: ListItem;

  constructor() {
  }
}
