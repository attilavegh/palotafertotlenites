import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { ContentService } from '../../services/content/content.service';
import { CompatibilityService } from '../../services/compatibility/compatibility.service';

@Component({
  selector: 'pf-teaser-content',
  templateUrl: './teaser-content.component.html',
  styleUrls: ['./teaser-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeaserContentComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('background') background: ElementRef;

  shouldShowOfferButton = true;

  private routerSubscription: Subscription;

  constructor(private contentService: ContentService,
              private router: Router,
              private changeDetectionRef: ChangeDetectorRef,
              private compatibility: CompatibilityService,
              private renderer: Renderer2) {
  }

  ngOnInit() {
    this.routerSubscription = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
    ).subscribe((event: NavigationEnd) => {
      this.shouldShowOfferButton = !event.urlAfterRedirects.includes(this.content.data.teaser.emailButton.href);
      this.changeDetectionRef.markForCheck();
    });
  }

  ngAfterViewInit() {
    this.compatibility.hasWebPSupport$.subscribe((hasWebPSupport: boolean) => {
      if (hasWebPSupport) {
        this.renderer.setStyle(this.background.nativeElement, 'background-image', 'url("../../../assets/images-webp/teaser.webp")');
      } else {
        this.renderer.setStyle(this.background.nativeElement, 'background-image', 'url("../../../assets/images/teaser.jpg")');
      }
    });
  }

  ngOnDestroy() {
    this.routerSubscription?.unsubscribe();
  }

  get content() {
    return this.contentService;
  }
}
