import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Image } from '../../services/content/content.model';

@Component({
  selector: 'pf-text-content',
  templateUrl: './text-content.component.html',
  styleUrls: ['./text-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextContentComponent {

  @Input() title: string;
  @Input() paragraphs: string[];
  @Input() icon: string;
  @Input() titleImage: Image;

  constructor() {
  }
}
