import { Directive, ElementRef, EventEmitter, Input, Output } from '@angular/core';

@Directive({
  selector: '[pfFragment]'
})
export class FragmentDirective {

  @Input() threshold = 0;
  @Input() fistElement: boolean;
  @Input() lastElement: boolean;

  @Output() reachedFragment = new EventEmitter<boolean>();

  constructor(private elementRef: ElementRef) {
    window.addEventListener('scroll', () => {
      const pageOffset = window.pageYOffset + 65;
      const elementOffset = this.fistElement ? 0 : elementRef.nativeElement.offsetTop - this.threshold;

      if (pageOffset >= elementOffset) {
        this.reachedFragment.emit(true);
      }

      if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight && this.lastElement) {
        this.reachedFragment.emit(true);
      }
    });

    setTimeout(() => {
      window.dispatchEvent(new Event('scroll'));
    });
  }
}
