import { IntersectionEvent } from './intersection-event.model';
import { IntersectionObserverMock } from './intersection-observer.mock';

export class IntersectionEventMock implements IntersectionEvent {
  entry: IntersectionObserverEntry;
  observer = new IntersectionObserverMock();

  constructor(entry: Partial<IntersectionObserverEntry>) {
    this.entry = entry as IntersectionObserverEntry;
  }
}
