export interface IntersectionEvent {
  entry: IntersectionObserverEntry;
  observer: IntersectionObserver;
}
