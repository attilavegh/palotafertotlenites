export class IntersectionObserverMock implements IntersectionObserver {
  root = null;
  rootMargin = '';
  thresholds = [1];

  observe() {}
  unobserve() {}
  disconnect() {}
  takeRecords() {
    return {} as IntersectionObserverEntry[];
  }
}
