import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { IntersectionDirective } from './intersection.directive';
import { IntersectionService } from '../../services/intersection/intersection.service';
import { IntersectionEventMock } from './intersection-event.mock';
import { IntersectionServiceMock } from '../../services/intersection/intersection.service.mock';

@Component({
  template: ` <div pfIntersection (intersection)="onIntersection($event)" class="target">Target element</div> `
})
class WrapperComponent {
  onIntersection(_: any) {}
}

describe('IntersectionDirective', () => {
  let fixture: ComponentFixture<WrapperComponent>;
  let wrapperComponent: WrapperComponent;
  let targetElement: DebugElement;
  let intersectionService: IntersectionService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WrapperComponent, IntersectionDirective],
      providers: [{ provide: IntersectionService, useClass: IntersectionServiceMock }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperComponent);
    wrapperComponent = fixture.componentInstance;
    targetElement = fixture.debugElement.query(By.css('.target'));
    intersectionService = TestBed.inject(IntersectionService);
  });

  describe('onInit', () => {
    it('should start observing intersection on host element', () => {
      spyOn(intersectionService, 'observe').and.stub();

      fixture.detectChanges();

      expect(intersectionService.observe).toHaveBeenCalledWith(targetElement.nativeElement);
      expect(wrapperComponent).toBeTruthy();
    });
  });

  describe('intersection output', () => {
    xit('should emit intersections on the host element', () => {
      spyOn(wrapperComponent, 'onIntersection');
      fixture.detectChanges();

      const intersectionEventMock = new IntersectionEventMock({ target: targetElement.nativeElement });
      intersectionService.intersection$.next(intersectionEventMock);

      fixture.detectChanges();

      expect(wrapperComponent.onIntersection).toHaveBeenCalledWith(intersectionEventMock);
    });

    it('should NOT emit intersections on other targets', () => {
      spyOn(wrapperComponent, 'onIntersection');
      fixture.detectChanges();

      const otherTarget = document.createElement('span');
      const intersectionEventMock = new IntersectionEventMock({ target: otherTarget });
      intersectionService.intersection$.next(intersectionEventMock);

      fixture.detectChanges();

      expect(wrapperComponent.onIntersection).not.toHaveBeenCalled();
    });
  });
});
