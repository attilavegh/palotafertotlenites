import { Directive, ElementRef, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';

import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { IntersectionService } from '../../services/intersection/intersection.service';
import { IntersectionEvent } from './intersection-event.model';

@Directive({
  selector: '[pfIntersection]'
})
export class IntersectionDirective implements OnInit, OnDestroy {
  @Output() intersection = new EventEmitter<IntersectionEvent>();
  private intersectionSubscription: Subscription;

  constructor(private elementRef: ElementRef, private intersectionService: IntersectionService) {
  }

  ngOnInit() {
    this.intersectionSubscription = this.intersectionService.intersection$
      .pipe(
        filter(({ entry: { target, isIntersecting } }) => target === this.elementRef.nativeElement && isIntersecting),
      ).subscribe(this.intersection);

    this.intersectionService.observe(this.elementRef.nativeElement);
  }

  ngOnDestroy() {
    this.intersectionService.unobserve(this.elementRef.nativeElement);
    // TODO: figure out if we really need to unsubscribe here
    this.intersectionSubscription.unsubscribe();
  }
}
