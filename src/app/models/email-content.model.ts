import { RecaptchaToken } from './recaptcha.models';

export interface EmailContent {
  name: string;
  phone: string;
  email: string;
  subject: string;
  text: string;
}

export interface EmailContentResponse {
  from: string;
  to: string;
  subject: string;
  html: string;
}

export interface EmailRequestBody {
  name: string;
  email: string;
  subject: string;
  htmlContent: string;
  siteIdentifier: string;
  recaptchaToken: RecaptchaToken;
}
