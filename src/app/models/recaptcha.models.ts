export interface Recaptcha {
  ready: (callbackFn: () => void) => void;
  execute: (siteKey: string, params: { [key: string]: string }) => Promise<RecaptchaToken>;
}

export interface RecaptchaToken {
  token: string;
}
