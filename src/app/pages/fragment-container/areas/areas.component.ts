import { Component } from '@angular/core';
import { ContentService } from '../../../services/content/content.service';

@Component({
  selector: 'pf-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent {

  constructor(private contentService: ContentService) {
  }

  get content() {
    return this.contentService.data.areas;
  }
}
