import { Component } from '@angular/core';
import { ContentService } from '../../../services/content/content.service';

@Component({
  selector: 'pf-disinfectant',
  templateUrl: './disinfectant.component.html',
  styleUrls: ['./disinfectant.component.scss']
})
export class DisinfectantComponent {

  constructor(private contentService: ContentService) {}

  get content() {
    return this.contentService.data.disinfection;
  }
}
