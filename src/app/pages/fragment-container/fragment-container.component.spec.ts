import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FragmentContainerComponent } from './fragment-container.component';
import { ServicesComponent } from './services/services.component';
import { DisinfectantComponent } from './disinfectant/disinfectant.component';
import { AreasComponent } from './areas/areas.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('FragmentContainerComponent', () => {
  let component: FragmentContainerComponent;
  let fixture: ComponentFixture<FragmentContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ FragmentContainerComponent, ServicesComponent, DisinfectantComponent, AreasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FragmentContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
