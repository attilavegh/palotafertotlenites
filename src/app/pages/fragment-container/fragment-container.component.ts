import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

import { ContentService } from '../../services/content/content.service';
import { FragmentNavigationService } from '../../services/fragment-navigation/fragment-navigation.service';

@Component({
  selector: 'pf-fragment-container',
  templateUrl: './fragment-container.component.html',
  styleUrls: ['./fragment-container.component.scss']
})
export class FragmentContainerComponent implements AfterViewInit {

  @ViewChild('service', { static: true }) service: ElementRef;

  constructor(private contentService: ContentService,
              private fragmentNavigation: FragmentNavigationService) {
  }

  ngAfterViewInit() {
  }

  onFragmentReached(fragment: string) {
    this.fragmentNavigation.onFragmentChange(fragment);
  }

  get fragments() {
    return this.contentService.pages.fragments;
  }
}
