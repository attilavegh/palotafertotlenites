import { Component } from '@angular/core';
import { ContentService } from '../../../services/content/content.service';

@Component({
  selector: 'pf-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent {

  constructor(private contentService: ContentService) {}

  get content() {
    return this.contentService.data.services;
  }
}
