import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { OfferRequestComponent } from './offer-request.component';

import { InputFieldComponent } from '../../components/input-field/input-field.component';
import { ButtonComponent } from '../../components/button/button.component';
import { ActionFeedbackComponent } from '../../components/action-feedback/action-feedback.component';

describe('OfferRequestComponent', () => {
  let component: OfferRequestComponent;
  let fixture: ComponentFixture<OfferRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule
      ],
      declarations: [
        OfferRequestComponent,
        InputFieldComponent,
        ButtonComponent,
        ActionFeedbackComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
