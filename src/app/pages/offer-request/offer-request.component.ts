import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { catchError, finalize, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

import { EmailService } from '../../services/email/email.service';
import { ContentService } from '../../services/content/content.service';
import { RecaptchaService } from '../../services/recaptcha/recaptcha.service';
import { markFormAsDirty, resetForm } from '../../utils/form.utils';
import { RecaptchaToken } from '../../models/recaptcha.models';

@Component({
  selector: 'pf-offer-request',
  templateUrl: './offer-request.component.html',
  styleUrls: ['./offer-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OfferRequestComponent {
  loading = false;
  status = { showFeedback: false, error: false };

  nameControl = new FormControl('', [Validators.required]);
  phoneControl = new FormControl('');
  emailControl = new FormControl('', [Validators.required, Validators.email]);
  subjectControl = new FormControl('', [Validators.required]);
  textControl = new FormControl('', [Validators.required]);

  form = new FormGroup({
    name: this.nameControl,
    phone: this.phoneControl,
    email: this.emailControl,
    subject: this.subjectControl,
    text: this.textControl
  });

  constructor(private emailService: EmailService,
              private recaptchaService: RecaptchaService,
              private contentService: ContentService,
              private changeDetectorRef: ChangeDetectorRef) {
  }

  onSubmit() {
    this.status = { showFeedback: false, error: false };

    if (this.form.valid) {
      this.loading = true;
      this.form.disable();
      this.sendMail();
    } else {
      markFormAsDirty(this.form);
    }
  }

  private sendMail() {
    this.recaptchaService.analyze().pipe(
      switchMap((token: RecaptchaToken) => this.emailService.send(this.form.value, token)),
      tap(() => {
        resetForm(this.form);
        this.status = { showFeedback: true, error: false };
      }),
      catchError(() => {
        this.status = { showFeedback: true, error: true };
        return of();
      }),
      finalize(() => {
        this.loading = false;
        this.form.enable();
        this.changeDetectorRef.detectChanges();
      })
    ).subscribe();
  }

  get content() {
    return this.contentService.data.offerRequest;
  }
}
