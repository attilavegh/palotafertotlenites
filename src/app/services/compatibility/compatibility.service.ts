import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CompatibilityService {

  hasWebPSupport$ = this._hasWebPSupport();

  constructor() {
  }

  private _hasWebPSupport(): Observable<boolean> {
    const webPSupport$ = new Observable<boolean>((observer) => {
      const imageContent = 'data:image/webp;base64,UklGRiIAAABXRUJQVlA4IBYAAAAwAQCdASoBAAEADsD+JaQAA3AAAAAA';

      const image = new Image();
      image.src = imageContent;

      image.addEventListener('load', () => {
        observer.next((image.width > 0) && (image.height > 0));
        observer.complete();
      });

      image.addEventListener('error', () => {
        observer.next(false);
        observer.complete();
      });
    });

    const webPSupportCacheKey = 'webp_supported';
    const webPSupportTimestampCacheKey = 'webp_supported_ts';

    let cachedSupport = localStorage.getItem(webPSupportCacheKey);
    let cachedSupportTs = localStorage.getItem(webPSupportTimestampCacheKey);

    const currentDate = Date.now();
    const oneMonthInMs = 2629800000;
    const timestampCreationDate = !!cachedSupportTs ? Number.parseInt(cachedSupportTs, 10) : null;

    const isExpired = cachedSupport === 'false' && !!timestampCreationDate && currentDate > timestampCreationDate + oneMonthInMs;

    if (isExpired || !timestampCreationDate || !cachedSupport) {
      localStorage.removeItem(webPSupportCacheKey);
      localStorage.removeItem(webPSupportTimestampCacheKey);

      cachedSupport = null;
      cachedSupportTs = null;
    }

    return cachedSupport
      ? of(cachedSupport === 'true').pipe(shareReplay(1))
      : webPSupport$.pipe(
        tap((hasWebPSupport: boolean) => {
          localStorage.setItem(webPSupportCacheKey, hasWebPSupport.toString());
          localStorage.setItem(webPSupportTimestampCacheKey, Date.now().toString());
        }),
        shareReplay(1)
      );
  }
}
