export interface Pages {
  urls: MenuItem[];
  fragments: {
    services: string;
    disinfection: string;
    areas: string;
  };
}

export interface PageContent {
  header: HeaderContent;
  teaser: TeaserContent;
  services: {
    textContent: TextContent;
    imageList: ImageContent[];
  };
  disinfection: {
    textContent: TextContent;
    listContent: ListItem[];
  };
  areas: {
    textContent: TextContent;
    imageList: ImageContent[];
  };
  offerRequest: OfferRequest;
  footer: FooterContent;
}

export interface ImageContent {
  imageName: string;
  title: string;
  text?: string;
}

export interface OfferRequest {
  title: string;
  button: string;
  labels: {
    name: string;
    phone: string;
    email: string;
    subject: string;
    text: string;
    error: string;
    success: string;
  };
}

export interface HeaderContent {
  items: MenuItem[];
}

export interface IframeConfig {
  title: string;
  src: string;
  parentElement: HTMLDivElement;
  injectionDelay?: number;
  width?: string;
  height?: string;
  allow?: string;
}

export interface Image {
  filename: string;
  alt: string;
}

export interface MenuItem {
  text: string;
  href: string;
  fragmentName?: string;
}

export interface TeaserContent {
  title: TitleFragment[];
  subtitles: string[];
  emailButton: ButtonContent;
  phoneButton: ButtonContent;
}

export interface TitleFragment {
  text: string;
  highlighted: boolean;
}

export interface ListItem {
  title: string;
  paragraphs: string[];
  icon?: string;
  highlighted?: boolean;
}

export interface TextContent {
  title: string;
  paragraphs?: string[];
  icon?: string;
  titleImage?: Image;
}

export interface FooterContent {
  sections: FooterSection[];
  copyright: Copyright;
}

interface Copyright {
  label: string;
  creditBegin: string;
  creditAfter: string;
  creditDescription: string;
}

export interface FooterSection {
  title: string;
  content: FooterSectionContent[];
  type: 'text' | 'image' | 'iframe';
}

export interface IFrame {
  src: string;
  title: string;
  width: string;
  height: string;
}

export interface FooterSectionContent {
  text: string;
  icon?: string;
  hasLink?: boolean;
  href?: string;
  fragmentName?: string;
  external?: boolean;
  contactInfo?: boolean;
  width?: number;
  iframe?: IFrame;
}

export interface ButtonContent {
  text: string;
  href: string;
}
