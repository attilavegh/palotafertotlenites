import { Injectable } from '@angular/core';
import { PageContent, Pages } from './content.model';

@Injectable({
  providedIn: 'root'
})
export class ContentService {
  private sharedLabels = {
    name: 'Palotafertőtlenítés',
    mobile: '+36 70 589 4255',
    email: 'info@palotafertotlenites.hu',
  };

  pages: Pages = {
    urls: [
      { text: 'Szolgáltatások', href: '/', fragmentName: 'szolgaltatasok' },
      { text: 'Fertőtlenítés', href: '/', fragmentName: 'fertotlenites' },
      { text: 'Területek', href: '/', fragmentName: 'teruletek' },
      { text: 'Ajánlatkérés', href: '/ajanlatkeres' }
    ],
    fragments: {
      services: 'szolgaltatasok',
      disinfection: 'fertotlenites',
      areas: 'teruletek'
    }
  };

  data: PageContent = {
    header: {
      items: this.pages.urls,
    },
    teaser: {
      title: [
        { text: 'Palota', highlighted: true },
        { text: 'fertőtlenítés', highlighted: false }
      ],
      subtitles: ['Fertőtlenítés', 'alaposan', 'szakszerűen'],
      emailButton: { text: 'Ajánlatkérés', href: '/ajanlatkeres' },
      phoneButton: { text: this.sharedLabels.mobile, href: `tel:${this.sharedLabels.mobile}` }
    },
    services: {
      textContent: {
        title: 'Szolgáltatásaink',
        paragraphs: [
          'Különböző területek eltérő szennyezettséggel rendelkezhetnek, ezért fontos, hogy az alapos fertőtlenítés a napi szintű feladatok között szerepeljen. A fertőtlenítést lehetőség szerint az elérhető legtisztább felületen kell végrehajtani annak érdekében, hogy az eredmény kiváló legyen. Csapatunk a SARS-COV-2 T-T PROTOKOLL rendszerét alkalmazva végzi tevékenységét annak érdekében, hogy a szolgáltatásaink a lehető legalaposabbak legyenek az Ön biztonsága és elégedettsége érdekében.',
          'Alább találja az elérhető és megrendelhető fertőtlenítési szolgáltatásainkat.'
        ]
      },
      imageList: [
        {
          imageName: 'hidegkod',
          title: 'Hidegködképzés',
          text: 'Gyors és hatékony fertőtlenítő eljárás, mely elpusztítja a kórokozókat, de nem tesz kárt a felületekben. A ködképzés egyaránt alkalmazható zárt, illetve nyílt területek fertőtlenítésére. Biztonságosan alkalmazható mindenhol. A 20-30 mikron méretű cseppekből álló ködösítése által minden felület könnyedén fertőtleníthető. A ködösítés végeztével a felületen nem kell elvégezni egyéb takarítási feladatot, a 2-3 óra behatási idő után a terület használatba vehető.'
        },
        {
          imageName: 'eg',
          title: 'Ózonos fertőtlenítés',
          text: 'Az ózon az egyik legerősebb oxidáló és fertőtlenítő anyag, ami 600-szor hatékonyabb fertőtlenítő hatású, mint a klór. Az oxidálási folyamat az ózonnal olyan eredményes, hogy a mikroorganizmusok nem tudnak immunitást felépíteni. Ózongenerátor működtetésekor a szennyező anyagok ózonnal való találkozása során oxidációs reakciók lépnek fel, amely során a káros részecskék (vírusok, baktériumok, szagmolekulák) elpusztulnak, míg az ózon molekula oxigénné alakul át. Így a folyamat végén nem marad más, mint a tiszta, oxigéndús levegő.'
        },
      ]
    },
    disinfection: {
      textContent: {
        title: 'Fertőtlenítőszer',
        paragraphs: [
          'A szolgáltatásainknál felhasznált Nanosept Aqua® a jelenleg elérhető legmodernebb fertőtlenítőszerek új generációját képviseli.'
        ],
        titleImage: {
          filename: 'nanosept-logo',
          alt: 'Nanosept fertőtlenítő logo'
        }
      },
      listContent: [
        {
          title: 'Az aktív oxigén és az ezüst kolloid erejével',
          paragraphs: [
            'Baktérium-, gomba- és vírusölő hatását a felszabaduló aktív oxigénnek és az ezüst kolloid részecskéknek köszönheti.'
          ]
        },
        {
          title: 'Gyors és hatékony fertőtlenítés',
          paragraphs: [
            'A termék használatával a kezelt felületekre jutó és száradás után visszamaradó ezüst tartós hatást biztosít.'
          ],
          highlighted: true
        },
        {
          title: 'Elnyújtott fertőtlenített állapotot biztosít',
          paragraphs: [
            'A megelőzés érdekében is kiemelt szerepet kap a megfelelő és hosszantartó felületfertőtlenítés.'
          ]
        },
        {
          title: 'Sokoldalú felhasználás',
          paragraphs: [
            'Alkalmas családi házak fertőtlenítésére és intézmény szintű fertőtlenítési munkálatok elvégzésére is, beleértve az egészségügyet, mezőgadasági létesítményeket és vállalati, ipari területeket is.'
          ]
        },
        {
          title: 'Az emberi egészségre ártalmatlan',
          paragraphs: [
            'A nátrium-hipoklorit (hypo) tartalmú vegyszerek roncsolhatják a fertőtleníteni kívánt felületet, veszélyes bomlástermékeik károsíthatják a légzőszerveket. Ezzel szemben a Nanosept Aqua® használata során nem keletkeznek veszélyes, emberi egészségre káros bomlástermékek.'
          ],
          highlighted: true
        }
      ],
    },
    areas: {
      textContent: {
        title: 'Területek'
      },
      imageList: [
        { imageName: 'otthon', title: 'Otthon' },
        { imageName: 'oktatas', title: 'Oktatási intézmény' },
        { imageName: 'sport', title: 'Sportlétesítmény' },
        { imageName: 'egeszsegugy', title: 'Egészségügy' },
        { imageName: 'szorakozohely', title: 'Szórakozóhely' },
        { imageName: 'ettermek', title: 'Étterem' },
        { imageName: 'szallodak', title: 'Szállodák' },
        { imageName: 'ipari-terulet', title: 'Ipari terület' },
        { imageName: 'irodahaz', title: 'Irodaház' }
      ]
    },
    offerRequest: {
      title: 'Ajánlatkérés',
      button: 'Küldés',
      labels: {
        name: 'Név',
        phone: 'Telefonszám',
        email: 'Email',
        subject: 'Tárgy',
        text: 'Üzenet',
        error: 'Hiba történt az üzenet elküldése közben!',
        success: 'Köszönjük az érdeklődést!'
      }
    },
    footer: {
      sections: [
        {
          title: 'Elérhetőség',
          content: [
            { text: 'Telefon: ' + this.sharedLabels.mobile, href: `tel:${this.sharedLabels.mobile}`, external: true, contactInfo: true },
            { text: 'Email: ' + this.sharedLabels.email, href: `mailto:${this.sharedLabels.email}`, external: true, contactInfo: true },
          ],
          type: 'text'
        },
        {
          title: 'Oldalak',
          content: this.pages.urls,
          type: 'text'
        },
        {
          title: 'Kövessen minket',
          content: [
            {
              text: 'Facebook',
              iframe: {
                src: 'https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fpalotafertotlenites&tabs&width=250&height=64&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId=766181310477873',
                title: 'Facebook',
                width: '250',
                height: '70'
              }
            }
          ],
          type: 'iframe'
        }
      ],
      copyright: {
        label: '2020 © ' + this.sharedLabels.name + ' — Minden jog fenntartva.',
        creditBegin: 'Az oldalt ',
        creditAfter: ' készítette.',
        creditDescription: 'Webfejlesztő logó'
      }
    }
  };
}
