import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { EmailContent, EmailContentResponse, EmailRequestBody } from '../../models/email-content.model';
import { environment } from '../../../environments/environment';
import { RecaptchaToken } from '../../models/recaptcha.models';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  private siteIdentifier = environment.siteIdentifier;
  private emailUrl = environment.emailUrl;

  constructor(private httpClient: HttpClient) {
  }

  send(content: EmailContent, recaptchaToken: RecaptchaToken): Observable<EmailContentResponse> {
    return this.httpClient.post<EmailContentResponse>(this.emailUrl, {
      name: content.name,
      email: content.email,
      subject: 'Új üzenet: ' + content.subject,
      htmlContent: this.createHtmlContent(content),
      siteIdentifier: this.siteIdentifier,
      recaptchaToken
    } as EmailRequestBody);
  }

  private createHtmlContent(content: EmailContent) {
    const name = `<p><b>Név:</b> ${content.name}</p>`;
    const phone = `<p><b>Telefon:</b> ${content.phone}</p>`;
    const email = `<p><b>Email:</b> ${content.email}</p>`;
    const separatorLine = '<hr />';
    const newLine = '<br />';
    const text = `<p>${content.text.replace(new RegExp('\n', 'g'), newLine)}</p>`;
    const noReplyDisclaimer = '<p><i>Ez egy automatikus üzenet, kérem ne válaszoljon rá! A válaszát a fent megadott email címre küldje!</i></p>';

    return name
      + (!!content.phone ? phone : '')
      + email
      + separatorLine
      + newLine
      + text
      + newLine
      + separatorLine
      + noReplyDisclaimer;
  }
}
