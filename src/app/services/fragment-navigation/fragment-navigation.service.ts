import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { delay, filter, share, tap } from 'rxjs/operators';
import { merge, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FragmentNavigationService {

  blocked = false;
  private fragmentChanger$ = new Subject<string>();

  activatedFragment$ = this.activatedRoute.fragment.pipe(share());
  currentFragment$ = merge(this.activatedFragment$, this.fragmentChanger$);

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.navigateToFragmentOnLoad();
  }

  onFragmentChange(fragment: string) {
    this.fragmentChanger$.next(fragment);
  }

  private navigateToFragmentOnLoad() {
    this.activatedFragment$.pipe(
      filter(fragment => !!fragment),
      delay(0),
      tap((fragment) => {
        this.onFragmentChange(fragment);
        this.router.navigate(['/'], { fragment });
      }),
    ).subscribe();
  }
}
