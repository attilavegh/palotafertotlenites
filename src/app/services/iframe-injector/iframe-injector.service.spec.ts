import { TestBed } from '@angular/core/testing';

import { IframeInjectorService } from './iframe-injector.service';

describe('IframeInjectorService', () => {
  let service: IframeInjectorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IframeInjectorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
