import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { Observable } from 'rxjs';
import { delay, switchMap, take, tap } from 'rxjs/operators';

import { IframeConfig } from '../content/content.model';

export interface IframeInjectorServiceShape {
  inject(iframeConfig: IframeConfig): Observable<Event>;
}

@Injectable({
  providedIn: 'root'
})
export class IframeInjectorService implements IframeInjectorServiceShape {

  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  inject(config: IframeConfig): Observable<Event> {
    return this.createIframeElement(config).pipe(
      take(1),
      delay(config.injectionDelay || 0),
      tap((iframe: HTMLIFrameElement) => config.parentElement.appendChild(iframe)),
      switchMap((iframe: HTMLIFrameElement) => this.getScriptInsertionResult(iframe))
    );
  }

  private createIframeElement(config: IframeConfig) {
    return new Observable<HTMLIFrameElement>((observer) => {
      const iframe: HTMLIFrameElement = this.document.createElement('iframe');

      iframe.title = config.title;
      iframe.src = config.src;
      iframe.width = config.width;
      iframe.height = config.height;

      if (config.allow) {
        iframe.allow = config.allow;
      }

      observer.next(iframe);
      observer.complete();
    });
  }

  private getScriptInsertionResult(iframe: HTMLIFrameElement): Observable<Event> {
    return new Observable<Event>((observer) => {
      iframe.addEventListener('load', () => {
        observer.next();
        observer.complete();
      });

      iframe.addEventListener('error', () => {
        observer.error('Iframe cannot be loaded.');
      });
    });
  }
}
