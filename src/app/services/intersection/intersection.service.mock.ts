import { OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';

import { IntersectionService } from './intersection.service';
import { IntersectionEvent } from '../../directives/intersection/intersection-event.model';

export class IntersectionServiceMock implements Required<IntersectionService>, OnDestroy {
  intersection$ = new Subject<IntersectionEvent>();
  observe() {}
  unobserve() {}
  ngOnDestroy() {}
}
