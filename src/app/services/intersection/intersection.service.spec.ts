
import { IntersectionService } from './intersection.service';

export type PublicIntersectionService = Required<IntersectionService> & {
  intersectionObserver: IntersectionObserver;
};

describe('IntersectionService', () => {
  const element = document.createElement('span');
  let intersectionService: PublicIntersectionService;

  beforeEach(() => {
    intersectionService = (new IntersectionService() as any) as PublicIntersectionService;
  });

  it('should create an intersection observer', () => {
    expect(intersectionService.intersectionObserver).toBeTruthy();
  });

  describe('observe', () => {
    it('should call observe on IntersectionObserver', () => {
      spyOn(intersectionService.intersectionObserver, 'observe').and.stub();

      intersectionService.observe(element);

      expect(intersectionService.intersectionObserver.observe).toHaveBeenCalledWith(element);
    });
  });

  describe('unobserve', () => {
    it('should call observe on IntersectionObserver', () => {
      spyOn(intersectionService.intersectionObserver, 'unobserve').and.stub();

      intersectionService.unobserve(element);

      expect(intersectionService.intersectionObserver.unobserve).toHaveBeenCalledWith(element);
    });
  });

  describe('ngOnDestroy', () => {
    it('should disconnect from the observer', () => {
      spyOn(intersectionService.intersectionObserver, 'disconnect').and.stub();

      intersectionService.ngOnDestroy();

      expect(intersectionService.intersectionObserver.disconnect).toHaveBeenCalled();
    });
  });
});
