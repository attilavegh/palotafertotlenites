import { Injectable, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';

import { IntersectionEvent } from '../../directives/intersection/intersection-event.model';

@Injectable({
  providedIn: 'root'
})
export class IntersectionService implements OnDestroy {
  private readonly intersectionObserverInit = {
    threshold: 0
  };
  intersection$ = new Subject<IntersectionEvent>();
  // a single intersection observer with many targets is more performant than many observers (one per target)
  // the limitation is that the observer options (root, threshold) are shared
  private intersectionObserver?: IntersectionObserver;

  constructor() {
    this.intersectionObserver = new IntersectionObserver((entries, observer) => {
      entries.forEach((entry) => {
        this.intersection$.next({ entry, observer });
      });
    }, this.intersectionObserverInit);
  }

  ngOnDestroy() {
    if (this.intersectionObserver) {
      this.intersectionObserver.disconnect();
    }
  }

  observe(target: Element) {
    if (this.intersectionObserver) {
      this.intersectionObserver.observe(target);
    }
  }

  unobserve(target: Element) {
    if (this.intersectionObserver) {
      this.intersectionObserver.unobserve(target);
    }
  }
}
