import { Injectable } from '@angular/core';

import {Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { switchMap, take } from 'rxjs/operators';

import { ScriptInjectorService } from '../script-injector/script-injector.service';
import { environment } from '../../../environments/environment';
import { Recaptcha, RecaptchaToken } from '../../models/recaptcha.models';

declare var grecaptcha: Recaptcha;

@Injectable({
  providedIn: 'root'
})
export class RecaptchaService {

  private readonly siteKey = environment.recaptchaSiteKey;
  private readonly recaptchaScriptUrl = `https://www.google.com/recaptcha/api.js?render=${this.siteKey}`;

  constructor(private scriptInjector: ScriptInjectorService) {
  }

  analyze(): Observable<RecaptchaToken> {
    return this.injectRecaptchaScript().pipe(
      switchMap(() => this.initRecaptcha()),
      switchMap(() => this.executeCaptcha())
    );
  }

  private initRecaptcha(): Observable<void> {
    return new Observable((observer) => {
      grecaptcha.ready(() => {
        observer.next();
        observer.complete();
      });
    });
  }

  private executeCaptcha(): Observable<RecaptchaToken> {
    return fromPromise(grecaptcha.execute(this.siteKey, { action: 'submit' })).pipe(take(1));
  }

  private injectRecaptchaScript(): Observable<Event> {
    return this.scriptInjector.inject(this.recaptchaScriptUrl);
  }
}
