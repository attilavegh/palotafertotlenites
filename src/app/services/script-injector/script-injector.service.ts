import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { Observable } from 'rxjs';

export interface ScriptInjectorServiceShape {
  inject(fromPath: string): Observable<Event>;
}

@Injectable({
  providedIn: 'root'
})
export class ScriptInjectorService implements ScriptInjectorServiceShape {
  constructor(@Inject(DOCUMENT) private document: Document) {}

  inject(fromPath: string): Observable<Event> {
    const script = this.createScriptElement(fromPath);
    this.appendScriptElement(script);
    return this.getScriptInsertionResult(script);
  }

  private appendScriptElement(script: HTMLScriptElement) {
    this.document.body.appendChild(script);
  }

  private createScriptElement(fromPath: string) {
    const script: HTMLScriptElement = this.document.createElement('script');

    script.async = true;
    script.type = 'text/javascript';
    script.src = fromPath;

    return script;
  }

  private getScriptInsertionResult(script: HTMLScriptElement): Observable<Event> {
    return new Observable<Event>((observer) => {
      script.addEventListener('load', () => {
        observer.next();
        observer.complete();
      });

      script.addEventListener('error', () => {
        observer.error('Script cannot be loaded.');
      });
    });
  }
}
