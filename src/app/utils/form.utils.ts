import { AbstractControl, FormControl, FormGroup } from '@angular/forms';

export function resetForm(form: FormGroup) {
  updateFormControls(form, (control) => {
    control.reset();
    control.markAsPristine();
  });
}

export function markFormAsDirty(form: FormGroup) {
  updateFormControls(form, (control) => control.markAsDirty());
}

function updateFormControls(form: FormGroup, callbackFn: (control: AbstractControl) => void) {
  Object.keys(form.controls)
    .map((key: string) => form.get(key))
    .forEach((control: FormControl) => {
      callbackFn(control);
      control.updateValueAndValidity();
    });
}
